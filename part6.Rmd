---
title: "Part 6"
author: "Chris Janaqi"
date: "8/8/2019"
output: html_document
---

# Statistics with vectors

Now that we know how to proccess our input data, it's time to
extract some statistical information from it.

## `sort` and `order`

One thing that one may want to do is to sort the values of a 
vector.

The functions `sort` and `order` does this job of sorting the
data.

We have the following relation for an input named `input`:

```r
sort(input) == input[order(input)]
```

The differences between the two functions is that `sort` do
the actual job of sorting the data, whereas `order` returns
the indices for the input vector in the right order for the
input to be sorted.

> In practice, you may want to use `order` in situation 
> where you want to sort multiple vectors according to one 
> variable

Here is an exemple of use for both:

```{r}
# simple exemple for the sort function
input <- as.integer(runif(10, 0, 100))
input

input.sorted <- sort(input)
input.sorted

# simple exemple for the order function
names <- c("Sheik", "Ganon", "Zelda", "Link")
height <- c(1.65, 1.90, 1.57, 1.76)

# I want to sort the data in alphabetical order

indices <- order(names)
names[indices]
# corresponding height
height[indices]
```

## Main statistics functions

`R` provides basically all statistics functions that you may want to use. I will use a normal distribution with mean 50 and standard deviation 5 to illustrate all those functions:

```{r}
# I take 100 occurences of this random variable
normal_distrib <- rnorm(500, 50, 5)
# I only print the first values to see what it looks like
head(normal_distrib)
```

The first function that you want to know is `summary`. It provides relevant information about your dataset, treating it as statistical values:

```{r}
summary(normal_distrib)
```

Then here are the other functions that you need to know to analyse datasets:

```{r}
# the mean of the dataset
mean(normal_distrib)
# the median
median(normal_distrib)
# the minimum
min(normal_distrib)
# the maximum
max(normal_distrib)
# the quartiles
quantile(normal_distrib)
# the variance
var(normal_distrib)
# the standard deviation
sd(normal_distrib)
```

> All those functions have additional optional arguments, that 
> you may need. Don't forget the `help` tab or function to 
> access those informations. 
>
> For exemple you may want the deciles of the distribution
> and not the quartile. For this, additionnal argument to the 
> `quantile` function needs to be provided.

We can then vizualize the data we have and see how it fits the density functions used to generate the sample:

```{r}
# We plot the histgram 
hist(normal_distrib, freq = F, ylim = c(0, 0.1))

# We add on top the density curve of the normal law
x <- seq(30,65, 0.1)
lines(x, dnorm(x, 50, 5), col = 'blue')

# we create the legend for our graph
legend(
  30, 0.1,  # the position on the graph
  legend = c("histogram", "density"), # labels 
  col = c("black", "blue"), # color on the lines
  lty = c(1, 1) # line types
)
```

## Application {.tabset .tabset-fade}

### Exercise {-}

Let's take some real data and try to do simple analysis on them!
I propose we take a look at data from the [insee](https://www.insee.fr/).
I chose to take raw data of mean annual wages for Executives and 
Intellectual Occupations for men, women, and all 
([source](https://www.insee.fr/fr/statistiques/serie/001665101+001665102+001665103)).

You can download the raw data here: [executive_wages.csv](data/executive_wages/executive_wages.csv)
and the complement [caract.csv](data/executive_wages/caract.csv).

1. From those data, try to plot on the same graph the evolution of the annual wage for men, women, and all.

> This exercise is *hard* and you will stumble upon many obstacles 
before reaching to an actual result. 

2. From there try to extract the proportion of men working in this field and plot the evolution of this proportion over the years.

### Hints {-}

* The first thing you need to do is to import the data to RStudio. You can use the function `read.csv` to do so, but it won't work just with that. French csv files cannot use `,` as separator because it's use to write decimal numbers. French csv uses `;` to separate the values. Try to find a way to specify that to the `read.csv` function.

* Try to extract the meaningfull values inside vectors (one for men, one for women, one for all) or a `matrix` containing all those data. You can remove specific elements of a vector with the following syntax : 
```r
vec[-1] # will remove the first element of vec
```

You can also select the specific values that you want.

* To plot the graph you need access to the years that are inside the initial data. Moreover once you have the x axis, you want to use the `lines` function to plot several curves on the same graph. You will see that it's not enough, because the lines that you will add will not be shown perfectly into the graph. You need to specify the correct range for the y axis to fit the 3 curves at the same time with the `ylim` parameter of the `plot` function.

### Solutions {-}

```{r, echo=FALSE}

```

You can download my full solution [here](wages_analysis.R). It's commented but I still recommand you to read through the following explanation to fully understand my approach.

To do this analysis, I start by importing the data to RStudio. As I pointed out in the first hint, french csv use the semicolon `;` to separate values and not comas `,` so I need to specify that to the `read.csv` function as following:

```{r, echo=FALSE}
rm(list = ls())
```

```{r}
# I specify the path and the separator
wages.raw <- read.csv("data/executive_wages/executive_wages.csv", sep = ";")
```

Then we need to isolate the appropriate values for our analysis, my approach is to use named vectors and matrices. For this matter the only relevant data is from the 3rd line and on columns 2, 4, and 6. The code columns after each value specify the statistical information contained inside, where `A` means normal value. All the labels can be found inside the `caract.csv` file.

I will propose two equivalent ways of doing this:

The first will use the range notation `start:stop` and the `seq(start, stop, step)` function:

```{r, eval=FALSE}
wages.data <- as.matrix(wages.raw[3:nrow(wages.raw), seq(2, ncol(wages.raw), 2)])
```

> `nrow` and `ncol` return respectively the number of lines and number of columns
> of a matrix or a dataframe.
>
> Also for dataframes or matrices, accessing values is with the syntax 
> `matrix[line, column]`

What the command above says is: "Take from `wages.raw` the lines from 3 to the end, and take one column out of two from the second to the end. Then convert the result into a matrix."

One other way to do that is to say: "Remove the first two lines, and take the column 2, 4 and 6". This is a bit clearer but also less general.

```{r}
wages.data <- as.matrix(wages.raw[c(-1, -2), c(2, 4, 6)])
```

Then we need to specify the names for the rows and for the columns of this matrix. The column names will be `"all"`, `"men"`, and `"women"`. The lines will be the years.

We have in our data names incredibly long that we need to change:

```{r}
colnames(wages.data) <- c("men", "women", "all")
rownames(wages.data) <- wages.raw$Label[3:nrow(wages.raw)]
```

> I'm taking the years as in the specified in the Label column of the initial 
> data to ensure coherence.

Now our data looks like this:

```{r}
wages.data
```

Pretty neat, huh! Now I see two problems that remains with this data, but it's very minor, I will still show you how to resolve them.

The first problem is that the data is ordered in decreasing order for the year, and I would want the data to be sorted in increasing order.

To do that we need the `order` function discussed above!

```{r}
wages <- wages.data[order(as.integer(rownames(wages.data))),]
```

Several things are to notice here.
The first is that I need to convert the rownames into integer.
Well... I don't *need* to do that, but by doing this I'm assuring
The data is sorted as I want.

> Sorting character sorts them alphabetically so 9 is sorted after 10 !
> Be carefull of the type of your data because it can produce unexpected results.
>
> To have the number ordering, you should always manipulate integer or floats.

By leaving the column specification empty, I take all the values.

> I still need to put the coma `,` though!

The second problem is even less important! It's just for convieniance, but now that my matrix is named I can specify the names of the data I want instead of the indices, which is more clear and convenient. But now I access my matrix by doing `wages["year", "sex"]` but I would prefer to do it the other way (I know, it's such a small detail). Still it may be useful to you in the future!

```{r}
wages <- aperm(wages)
# let's check that it worked
wages[, as.character(1995:1999)]
```

Now we're ready to plot our data! Finally! As I want to put the 3 curves one the same plot I need to specify the yaxis range beforehand otherwise it will scale only to the first curve I draw.

```{r}
ymin <- min(wages) # global minimum
ymax <- max(wages) # global maximum
```

Now it's time to plot!

```{r}
# x axis
years <- as.integer(colnames(wages))

plot(
  years, wages["all",], # data
  type = "b", col = "limegreen", # line type and color
  main = "Average full time annual wage from 1995 to 2015 in France
  for men, women, and all Executive or Intellectual professions", # title
  xlab = "Years", # x axis label
  ylab = "Annual wage (€)", # y axis label
  ylim = c(ymin, ymax) # boundaries of the y axis to fit the 3 curves
)

lines(years, wages["men",], type = "b", col = "blue")
lines(years, wages["women",], type = "b", col = "red")

# specify a legend
legend(
  1995, ymax, # coordinate of the topleft point of the legend rectangle
  legend = c("Men", "All", "Women"), # labels to put in the legend
  lwd = 1.5, # width of the line to show with the labels
  col = c("blue", "limegreen", "red"), # color of the lines corresponding to the labels
)
```

Here we can see that the value for the men serie in 1997 is somehow wrong. It correspond perfectly to the value of the global serie of this year!

We'll correct it just after plotting the ratio of man in this field.

To plot this, we need to compute this ratio with the data we have. The idea here is to notice and to model how the global serie was calculated. The global wage is a *weighted average* of the man wage and the woman wage. The weights used is the proportion of each in this field! So if we call $c$ this ratio, we have the following formula:

$$
all = (1 - c) * women + c * men
$$

We can find the formula to calculate $c$ using only the wages:

$$
c = \frac{all - women}{men - women}
$$

This formula is very meaningful of the thing we want to access. But let's plot it then!

```{r}

# compute the ratio of man in this field
men.ratio <- (
  (wages["all",] - wages["women",]) 
  / (wages["men",] - wages["women",])
)

# plot the curve
plot(
  years, men.ratio,
  type = "b", col = "blue",
  main = "Ratio of men in France for Executive and Itellectual professions",
  xlab = "Years",
  ylab = "Ratio",
  ylim = c(0.5, 1)
)
```

Here we can still see the aberrant value of 1997. We need to correct it.
One way to do so is by taking the mean of the value before and after as we see that it's mainly a linear tendency.

```{r}
men.ratio['1997'] <- mean(men.ratio[c('1996', '1998')])

# Replot 
plot(
  years, men.ratio,
  type = "b", col = "blue",
  main = "Ratio of men in France for Executive and Itellectual professions (corrected)",
  xlab = "Years",
  ylab = "Ratio",
  ylim = c(0.5, 1)
)
```

We can use this corrected value to calculate an approximation of the average wage for men in 1997 like so:

```{r}
# correct the wage of 1997 accordingly
wages["men", "1997"] <- (
  wages["women", "1997"] 
  + (wages["all", "1997"] - wages["women", "1997"]) / men.ratio["1997"]
)
# Replot 

plot(
  years, wages["all",],
  type = "b", col = "limegreen",
  main = "Average full time annual wage from 1995 to 2015 in France
  for men, women, and all Executive or Intellectual professions (corrected)",
  xlab = "Years",
  ylab = "Annual wage (€)",
  ylim = c(ymin, ymax)
)


lines(years, wages["men",], type = "b", col = "blue")
lines(years, wages["women",], type = "b", col = "red")

legend(
  1995, ymax,
  legend = c("Men", "All", "Women"),
  lwd = 1.5,
  col = c("blue", "limegreen", "red"),
)
```

Here! We have beautiful datas and a beautiful tendency!

## Linear Model

From the data of the application before, we can build a linear model to model the evolution of the proportion of men in this field. Since we noticed the points are almost aligned, a linear model seems to be a good hypothesis for our data.

Building a linear model in `R` is very simple!

Just a reminder of the variables at our disposal:
```{r}
ls()
```

We're trying to explain the men ratio with regard to the year and 
to model this evolution linearly.

```{r}
model <- lm(men.ratio ~ years) 
summary(model)
```

This code means: Build a linear model (lm) of the men ratio explained by the years. This will return a Model (dataframe) that has several components.

> The tilde `~` in the formula means to explain the left side with the 
> right side

With this summary we can see that the fit is very good ($R^2 \approx 0.98$) Also it provides insights on the residuals. 

### More on linear models {-}

What is a linear model ? In this part we will explore the details of how it works. Let's say we have an input $X$ and an output $Y$. builing a linear model $Y \sim X$ means to find the line that fit best the graph. A line is defined by its slope and its y axis intercept. Let's call them $a$ for the slope and $b$ for the intercept. What we try to find is those $(a, b)$ such that the model minimize its error.

$$
  Y = aX + b + \epsilon
$$

Where epsilon is the error vector that we want to minimize. This is what R calls "residuals". This can be approached as a random variable as well this is why R provides the summary of this error at the begining of the model.

***

Let's plot the result of our model.

```{r}
plot(
  years, men.ratio,
  type = "b", col = "blue",
  main = "Ratio of men in France for Executive and Itellectual professions (corrected)",
  xlab = "Years",
  ylab = "Ratio",
  ylim = c(0.5, 1)
)

lines(years, model$fitted.values)
```

We can extract the slope of this model to see when we will reach the 50% threshold. 

```{r}
model$coefficients
```

We can see here that it decreases for 0.86% per year. That's not much but that's something!
