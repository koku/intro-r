---
title: "Part 7"
author: "Chris Janaqi"
date: "8/8/2019"
output: html_document
---

# Hypothesis Testing

```{r, echo=FALSE}
rm(list = ls())
```

In this part we will explore some hypothesis tests. The input data is a generated set of data of high jump. 
Please download the file [high_jump.RData](data/high_jump.RData).

To load this file into `RStudio` simply use the `load` function:

```{r}
load("data/high_jump.RData")
```

This file contains 4 named vectors, 2 for men and 2 for women. We have data on heights and on the performance of participants. Sadly some data is missing sometimes. So we will need to clean the data before doing anything else.

## Set operations {.tabset .tabset-fade}

Here we'll learn some basic operations to manipulate sets of values.

Lets concider two sets of values $X$ and $Y$ that contains the following elements:

```{r}
# some objects
X <- c("chair", "table", "lamp", "spoon", "tissue", "fork", "knife")

# some other objects
Y <- c("spoon", "fork", "knife", "bed", "sofa", "tv")
```

We can see here that there are element in common in those 2 sets.
The basics operations that one may want to do one those set of objects are:

 * obtain the set of all the objects that exists in those 2 sets without duplicates.
 * obtain the set of objects that are in both sets at the same time.
 * testing if an object belongs to one set
 * testing if 2 sets are identical
 * Remove elements from a set.
 
A set is a collection of element without duplicate value.

```{r}
# set of all object is the union of the two initial sets
union(X, Y) # it doesn't produce duplicates!

# common objects in both set is the intersection of those
intersect(X, Y)

# test if an object belongs to a set

is.element("lamp", X) # this also works with vector as first argument
is.element("lamp", Y)

# test if two sets are equals
setequal(X, Y) # doesn't care of the ordering inside vectors

# remove element from a set
setdiff(X, Y) # this operation is not symmetric
setdiff(Y, X)
```

With that being said, we can safely proceed to the data cleanup! We can see that not all participants have a height and a performance. We can only analyse people with both value!

### Exercise {-}

Using the data you imported earlier:

1. Store into new variables the height and performance of men and women than only have both values. Then sort those data alphabetically.

You should obtain at the end:

```{r, echo=FALSE}
men.common_name <- sort(intersect(
  names(men.height.raw), names(men.perf.raw)
))

women.common_name <- sort(intersect(
  names(women.height.raw), names(women.perf.raw)
))

# we keep only the values in common
men.perf <- men.perf.raw[men.common_name]
men.height <- men.height.raw[men.common_name]

women.perf <- women.perf.raw[women.common_name]
women.height <- women.height.raw[women.common_name]
```

```{r}
men.height
men.perf

women.height
women.perf
```

2. Plot the performance with regard to the height for all participants. Give different colors for men and women

### Solution {-}

1. We need to retrieve the names that are in common in boths sets of names for men and women.
For this matter, we need to use the `intersect` function.

```{r}
men.common_name <- sort(intersect(
  names(men.height.raw), names(men.perf.raw)
))

women.common_name <- sort(intersect(
  names(women.height.raw), names(women.perf.raw)
))

# we keep only the values in common
men.perf <- men.perf.raw[men.common_name]
men.height <- men.height.raw[men.common_name]

women.perf <- women.perf.raw[women.common_name]
women.height <- women.height.raw[women.common_name]

# showing the result to verify
men.height
men.perf

women.height
women.perf
```

2. For this a simple plot won't be enough, we need to do it with two steps, one for men, one for women. And we need to get the boudaries correct!

```{r}
# x axis boundaries
xmin <- min(men.height, women.height)
xmax <- max(men.height, women.height)

# y axis boundaries
ymin <- min(men.perf, women.perf)
ymax <- max(men.perf, women.perf)

plot(
  men.height, men.perf,
  col = "blue",
  main = "Performance of high jump with regard to the height",
  xlim = c(xmin, xmax), xlab = "Height (cm)",
  ylim = c(ymin, ymax), ylab = "High jump performance (cm)"
)

points(women.height, women.perf, col = "red")

legend(
  xmin, ymax, # topleft
  legend = c("men", "women"),
  pch = c(1, 1),
  col = c("blue", "red")
)
```

## Correlation Testing

With the processed data we can finaly do some hypothesis testing. The first one is to see if there is any corelation between the two measurement.

The function `cor(x, y)` allows you to calculate the correlation betwee the data $x$ and $y$. But this will do the expected result only if:

 * $x$ and $y$ are the same length.
 * each value of $x$ corresponds to the value of same index in $y$. 
 (this is what we did when we ordered our participants alphabetically).

We are in condition of using the `cor` function so let's try it.

```{r}
cor(women.height, women.perf)

cor(men.height, men.perf)

# correlation of all values
cor(c(men.height, women.height), c(men.perf, women.perf))
```

Here we see that individually, the correlation is very small, but when brought together, it becomes significant.

> You can compute multiple correlation at the same time by creating 
> matrices, It will perform the correlation column by column!

We can test the hypothesis height and performance are independant with a correlation test. In R, it's a simple call to the `cor.test` function. This tests the following hypothesis $H_1$ : "$corr(X, Y) \neq 0$"

```{r}
model.women <- cor.test(women.height, women.perf)

model.men <- cor.test(men.height, men.perf)

model.all <- cor.test(c(men.height, women.height), c(men.perf, women.perf))

# showing the p-value for each model
model.women$p.value
model.men$p.value
model.all$p.value
```

We see that for the two first models, we can't reject the Null hypothesis at a 5% ceil. And as expected we need to reject the null hypothesis for the whole dataset. It appears that the sex of the participant is correlated to the performance.

## T-Testing

T testing is a way to test if two variable $x$ and $y$ both following a normal distribution law have the same mean. More precisely, you can do a t test when:

 * You wish to test if $x \sim \mathcal{N}(\mu_x, \sigma_x^2)$ following a normal law has a mean of $\mu$. So the alternative hypothesis $H_1$ is one of the following proposition:
   - $H_1$: $''\mu_x = \mu''$ and $H_0$: $''\mu_x \neq \mu''$
   - $H_1$: $''\mu_x \geq \mu''$ and $H_0$: $''\mu_x < \mu''$
   - $H_1$: $''\mu_x \leq \mu''$ and $H_0$: $''\mu_x > \mu''$


 * You wish to test if $x \sim \mathcal{N}(\mu_x, \sigma_x^2)$ and $y \sim \mathcal{N}(\mu_y, \sigma_y^2)$ have the same mean. So the alternative hypothesis $H_1$ is one of the following proposition:
   - $H_1$: $''\mu_x = \mu_y''$ and $H_0$: $''\mu_x \neq \mu_y''$
   - $H_1$: $''\mu_x \geq \mu_y''$ and $H_0$: $''\mu_x < \mu_y''$
   - $H_1$: $''\mu_x \leq \mu_y''$ and $H_0$: $''\mu_x > \mu_y''$

> The test statistic is different if $\sigma_x = \sigma_y$ or if they are different!

Doing a t test in R is very simple! You just need to call the `t.test` function.

Example using our hight jump data:

Here our hypothesis could be, "boys jump higher than girls" or another one, "boys are taller than girls". Let's test both those hypothesis.

> Note that those hypothesis are not neutral! The way you formulate your hypothesis will lead you to test different things!

So my hypothesis are: "mean(men.height) >= mean(women.height)" and "mean(men.perf) >= mean(women.perf)"

Note that if I just wanted to show a difference my hypothesis could just be that those two means are different! We will do both so that we can see the difference!

```{r}
# test of performance greater or equal
test.perf.g <- t.test(men.perf, women.perf, alternative = 'greater')
test.perf.g$p.value

# test of equal mean for performance
test.perf.eq <- t.test(men.perf, women.perf, alternative = 'two.sided')
test.perf.eq$p.value
```

You can see in just this simple example that the p-value vary from one to another test just by changing our point of view on our data! Be carefull of how you phrase your hypothesis!

Let's do the same for the height

```{r}
# you can only specify the first letter in the alternative!
test.height.g <- t.test(men.height, women.height, alternative = 'g')
test.height.g$p.value

test.height.eq <- t.test(men.height, women.height, alternative = 't')
test.height.eq$p.value
```

Here again we see a difference from simple to double in the p value, and here the p value for the equality test is lower!

## Anova

The Analysis of Variance is a generalisation of the t test to multiple variables to see is there is a significant link between some of the initial variables. However it doesn't state which variables are linked. More precisely it's a way of testing if all the input variables have the same mean.

What I propose to do here, instead of seeing the anova calculation in R. Is to create a function that will create a matrix of t test from the columns of a dataframe.

You'll be able to use this function to see the pvalue of each couple of variables! I find that interesting!

Here's the code of the function^[source: [http://www.sthda.com/english/wiki/matrix-of-student-t-test](http://www.sthda.com/english/wiki/matrix-of-student-t-test)]:

```{r}
t.test.matrix <- function(mat, ...) {
  # convert input into proper matrix
  mat <- as.matrix(mat)
  n <- ncol(mat)
  # creating the output matrix
  p.mat <- matrix(nrow = n, ncol = n)
  # specifying the names of line and column
  colnames(p.mat) <- rownames(p.mat) <- colnames(mat)
  # the value on the diagonal is always one
  diag(p.mat) <- 1
  for (i in 1:(n - 1)) {
    for (j in (i + 1):n) {
      # testing i-th column with the j-th
      test <- t.test(mat[, i], mat[, j], ...)
      # the matrix is symmetric
      p.mat[j, i] <- signif(test$p.value, 3)
    }
  }
  # empties the upper triangle of the matrix
  p.mat[upper.tri(p.mat)] <- ""
  return(as.data.frame(p.mat))
}
```

You can download the function [here](t.test.matrix.R).

Let's see it in action:

```{r}
all.names <- sort(union(women.common_name, men.common_name))
# this will produce NA in the position where the data 
# is missing but all vectors will be the same length
# so we can build a dataframe out of it
men.height <- men.height[all.names]
men.perf <- men.perf[all.names]
women.height <- women.height[all.names]
women.perf <- women.perf[all.names]
# creating the input matrix
high.jump <- data.frame(
  Men.Height = men.height, Men.Perf = men.perf,
  Women.Height = women.height, Women.Perf = women.perf,
  row.names = all.names
)

p.mat <- t.test.matrix(high.jump)
p.mat
```

We see here that we find the same p values as what we calculated before! This example is not very interesting of course because most of those test doesn't make any sense! But still it can be very useful!

## Visualize your data

In R, it's very simple to compute and plot a correlation matrix starting from a dataframe. In this part we'll use two examples to demonstrate how easy it is!

But first we need to install some package! Please go to `Tools>Install Packages...` and install the packages `ggplot2`, `GGally`, and `RColorBrewer`.

If you have any troubles installing packages, it may be because you don't have `Rtools` installed on your machine. You can download Rtools here: [https://cran.r-project.org/bin/windows/Rtools/](https://cran.r-project.org/bin/windows/Rtools/)

Once you've installed those packages we can start!

> Those packages provide modern and beautiful data representations.
> They can be a bit complex to grasp at first but they are much more powerful than
> the basic plotting functions of R

First we need to say to R that we want to use the library!
```{r}
library(GGally)
```

We can then do our plots!
```{r}
# `mtcars` is one of the bundled dataset of R
# It's usefull when you want to try your script on different
# types of data!
head(mtcars)

ggcorr(mtcars[,1:7], label = TRUE)
```

It produces a beautiful correlation matrix!
But there's more! Let's look at the dataframe `iris` which contains data on 3 type of flowers.

```{r}
head(iris)
```

To see if there is dependecies or link between values, we can use the `ggpairs` function of the `GGally` package:

```{r, results=FALSE}
ggpairs(data = iris[, 1:4], mapping = aes(color = iris$Species, alpha = 0.7))
```

This graph has 3 parts : the upper part where there is the correlation matrix, the middle part where there is the histogram distribution of the variable, and the lower part plot one variable with regard to one other. Of course all this is highly customizable but we'll stay with default values for now!

## Real Anova with R

This part list the proper way to calculate type III Anova for neuropsychology studies. You will need to download the package `car` for this purpose.

The first thing you need to do is to request the package and change some options:

```r
# request our library
library(car)
# Set the model calculation options
options(contrasts=c("contr.sum", "contr.poly"))
```

### Anova 1 way

Let concider a dataframe `data` with column `y` and `A`.

```r
# create a model with aov function
fit <- aov(y ~ A, data=data)
# calculate type III anova
type3 <- Anova(fit, type="III")
```

### Anova 2 ways (between-subject factor)

Let concider a dataframe `data` with column `y` and `A, B`.

```r
# create a model with aov function
fit <- aov(y ~ A * B, data=data)
# calculate type III anova
type3 <- Anova(fit, type="III")
```

or

```r
# create a model with lm function
fit <- lm(y ~ A * B, data=data)
# calculate type III anova
type3 <- Anova(fit, type="III")
```

### Anova Repeated Measures

Let's consider a dataframe `data` with 8 subjects, A(2) * B(3) repeated measure
The input matrix should ressemble something like:
```{r, echo=FALSE}
cbind(subj = 1:8, A1_B1 = NA, A1_B2 = NA, A1_B3 = NA, A2_B1 = NA, A2_B2 = NA, A2_B3 = NA)
```

Except the `r NA` are actual values! With this correct input you can do a reapeted measures Anova with the following code:

```r
# define the dataframe if the intra-subject model
within_factor <- expand.grid(
  A=gl(2, 1, labels=c("P", "T")),
  B=gl(3, 1, labels=c("A", "B", "C"))
)
# You have to specify one for the right side of the formula
fit <- lm(data ~ 1)
# Here A and B refers to the column of within_factor to indicate the
# intra subject design
type3 <- Anova(fit, idata=within_factor, idesign=~A*B, type="III")
```

### Anova mixed design

This is for 1 between-subject variable and 2 within-subject

Same framework as before except `G` represent the group:

```{r, echo=FALSE}
cbind(subj = 1:8, Group = c(rep(1, 4), rep(2, 4)), A1_B1 = NA, A1_B2 = NA, A1_B3 = NA, A2_B1 = NA, A2_B2 = NA, A2_B3 = NA)
```

The code is basically the same as before except we have to do the linear model with regard to the group.

```r
# define the dataframe if the intra-subject model
within_factor <- expand.grid(
  A=gl(2, 1, labels=c("P", "T")),
  B=gl(3, 1, labels=c("A", "B", "C"))
)
# define the factor of the group
G <- factor(data$Group)
# compute the linear model with regard to the group
fit <- lm(data ~ G)
# Here A and B refers to the column of within_factor to indicate the
# intra subject design
type3 <- Anova(fit, idata=within_factor, idesign=~A*B, type="III")
summary(fit, multivariate=FALSE)
```

## Logistic Regression

The [logistic regression](https://en.wikipedia.org/wiki/Logistic_regression) is a method to build a model to explain a binary output ($e.g.$ ill/healthy, young/old) with a set of **independant** explainatory variables called *predictors*. This method can be extanded to a multiclass output variable ($i.e.$ a set of possible outcome not limited to 2) and is called [Multinomial logistic regression](https://en.wikipedia.org/wiki/Multinomial_logistic_regression).

> We will only treat simple binary output in this course

Once this model is built, one interesting feature that we can look is the number of false positive and the number of false negative produced by our model. We can produce a [confusion Matrix](https://en.wikipedia.org/wiki/Confusion_matrix) confronting our model to our data.

You will need to install the `caret` package to access the `confusionMatrix` function.

> Otherwise the `table` function also achieve this matrix except it gives less information

```{r}
library(ggplot2)
library(caret)
# Filter the setosa specie from the rest
iris.filter <- iris$Species != "setosa"
iris <- iris[iris.filter, ]
# Reducing the levels of the factor
iris$Species <- factor(iris$Species, levels = c("versicolor", "virginica"))
# Creating the success group virginica
iris$Group <- as.integer(iris$Species == "virginica")

# Building our model
model <- glm(Group ~ Sepal.Length + Sepal.Width + Petal.Length + Petal.Width,
             data=iris, family = "binomial")

summary(model)

# calculating the x axis
iris$x <- predict(model)
# plotting the result
ggplot() +
  # adding the points
  geom_point(data = iris, mapping = aes(x, Group, color=Species), alpha = 0.5) +
  # adding the line
  geom_line(mapping = aes(iris$x, model$fitted.values, linetype = "Model"), color = "cornflowerblue")

# building the confusion matrix 
pred <- factor(ifelse(model$fitted.values >= .5, "virginica", "versicolor"), levels= levels(iris$Species))
true <- iris$Species
confusionMatrix(pred, true)
```

